# Flockompass

Mobile app, client of flock server.

## Development

Install dependencies like so:

    $ pip install -r requirements.txt

[It might be necessary](https://github.com/kivy/buildozer/issues/769) to
manually download an android NDK. If so, go to [ndk
downloads](https://developer.android.com/ndk/downloads/) and get the appropiate
version. Put it somewhere (it could be inside this repository) and adjust
`android.ndk_path` in `buildozer.spec`. Relative paths are valid, so this is a
sensible value:

```ini
# (str) Android NDK directory (if empty, it will be automatically downloaded.)
android.ndk_path = ./android-ndk-r25b-linux
```

## Test building

Kivy apps are easily deployed and tested with
[buildozer](https://buildozer.readthedocs.io/en/latest/quickstart.html#init-and-build-for-android).

This command will compile the app, transfer it to a usb-connected
device and run it. Logs are shown on the console, hence the grep
python.

    $ buildozer -v android debug deploy run logcat | grep python
